#!/bin/bash

#----------------#
#     Colors     #
#----------------#
RED='\033[0;31m'
GREEN='\033[1;32m'
NC='\033[0m' 

#-----------------------------------------------#
#     Extensions of the files to be deleted     #
#-----------------------------------------------#
file_extension=(
    "aux"
    "bcf"
    "glg"
    "glo"
    "glo"
    "gls"
    "ist"
    "lof"
    "log"
    "lot"
    "out"
    "toc"
    "run.xml"
	"synctex.gz")


files=()

#----------------------------------------------------------#
#     Save in 'files' all files with matching extension    #
#----------------------------------------------------------#
for file in ${file_extension[@]}; do
    files+=($(ls *.$file 2>/dev/null))
done

#------------------------#
#     List the files     #
#------------------------#
echo -e "\nThe following files will be deleted:\n\n\
         \r${RED}${files[@]}\n\n\
         \r${NC}Are you sure? [y/n]"

read user_decision

if [ $user_decision == "y" ]; then
    echo -e "${GREEN}\
             \rThe files are deleted !\
             ${NC}"
    rm ${files[@]} 2>/dev/null
fi

exit 0

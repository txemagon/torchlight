## UML

```mermaid
classDiagram

    Game      o-- Interface
    Interface o-- Board
    Interface o-- Snake

    class Snake{
        -int length
        -QPoint []positions
        +Snake(int length)
    
        +move()     void
    
        +getPosition() QPoint
    
        +hasCrash()    bool
        +hasEatFruit() bool
    }
    class Keyboard{
        -QKeyEvent keyPressed
        -getKeyPressed() QKey
    }
    
    class Rule{
        +hasCrashed()  bool
        +hasEatFruit() bool
    }
    
    class Board{
        -int width
        -int heigth
        -QPoint fruit
    
        +Board()
        +getWidth()      int
        +getHeigth()     int
        +getFruitPoint() QPoint
    }
    
    class Interface {
        -Board board
        -Snake snake
    
        +Interface()
        +drawBoard() void
        +drawFruit() void
        +drawSnake() void
    
    }
    
    class Game {
    
        +Game()
    
    }
    
    class IPC {
    
    }
```

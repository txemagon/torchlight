import torch
import numpy as np

'''
    lh -> layer hidden
    lo -> layer output
    Descenso de Gradiante (Modo Batch)
'''

class Perceptron:

    weight_lh = []      # Pesos de la capa oculta
    weight_lo = []      # Pesos de la capa de salida
    bias_lh   = []      # Bias de la capa oculta
    bias_lo   = []      # Bias de la capa de salida
    l = []

    # Constructor
    def __init__ (self, size_weight_lh, size_weight_lo, size_inputs = 3):

        # Iniciarlizar los pesos y bias con valores aleatorios
        self.weight_lh = torch.tensor(np.random.normal(size = (size_inputs,size_weight_lh)), requires_grad=True)
        self.weight_lo = torch.tensor(np.random.normal(size = (size_weight_lh,size_weight_lo)), requires_grad=True)
        self.bias_lh   = torch.zeros(size_weight_lh, requires_grad=True)
        self.bias_lo   = torch.zeros(size_weight_lo, requires_grad=True)

    # Función de Propagación
    def annOutput (self, inputs):
        input_lh = inputs.mm(self.weight_lh) + self.bias_lh
        output_lh = input_lh.sigmoid()
        input_lo = output_lh.mm(self.weight_lo) + self.bias_lo
        return output_lh

    def sigmoid (self, input):
        return 1 / (1 + np.exp(-input))

    def cross_entropy(self, output, target):
        logits = output[torch.arange(len(output)), target]
        loss = - logits + torch.log(torch.sum(torch.exp(output), axis=-1))
        loss = loss.mean()
        return loss

    # Fase de entrenamiento
    def training (self, inputs, target, epochs, learning_rate):
        for epoch in range(epochs):
            #output = self.annOutput(torch.DoubleTensor(inputs))
            h = torch.DoubleTensor(inputs).mm(self.weight_lh) + self.bias_lh
            h_relu = h.sigmoid()
            output = h_relu.mm(self.weight_lo) + self.bias_lo
            output = h_relu

            print ("output", output, h, h_relu)
            error = output - torch.as_tensor(target)                                        # Calcular el Error en el valor de predicción
            #print ("output", output, "target", torch.as_tensor(target), "error", error)
            #error = self.cross_entropy(output, torch.as_tensor(target))                       # Calcular el Error en el valor de predicción
            #self.l.append(error.item())
            sumError = error.sum()                                              # Sumatorio error (comprobar la evolución)
            print ("Valor del Error: ", sumError)

            #error = error.mean()
            sumError.backward() # calcula todas las derivadas del error con respecto a todos los pesos, esto es gracias
            # al grafo computacional que ha contruido pytorch por debajo. backward aplica el algoritmo de backpropagation
            # y calcula todos los gradianes que son los valores con los cuales actualizaremos nuestros pesos y bias digamos que son las derivadas finales de anteriores ejemplos

            with torch.no_grad():
                self.weight_lh -= learning_rate * self.weight_lh.grad
                #self.weight_lo -= learning_rate * self.weight_lo.grad
                self.bias_lh -= learning_rate * self.bias_lh.grad
                #self.bias_lo -= learning_rate * self.bias_lo.grad

            # Es necesario poner a cero los gradiantes para que de esta manera no los acumule cada vez que llamamos a backward
                self.weight_lh.grad.zero_()
                #self.weight_lo.grad.zero_()
                self.bias_lh.grad.zero_()
                #self.bias_lo.grad.zero_()

            #print("Valor del Error: ", np.mean(self.l))

    # Calcular predicciones
    def predict (self, inputs):
        print (self.annOutput(torch.DoubleTensor(inputs).view(1,3)))
        return self.annOutput(torch.DoubleTensor(inputs).view(1,3)).detach().numpy()

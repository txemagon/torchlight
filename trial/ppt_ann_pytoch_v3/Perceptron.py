import torch
import numpy as np

'''
    lh -> layer hidden
    lo -> layer output
    Descenso de Gradiante (Modo Batch)
'''

class Perceptron:

    D_in, H, D_out = 3, 3, 3
    l = []

    # Constructor
    def __init__ (self):
        self.model = torch.nn.Sequential(
            torch.nn.Linear(self.D_in, self.H, bias = True),                                                            # bias=True por defecto
            torch.nn.ReLU(),                                                                                            # función de activacion
            torch.nn.Linear(self.H, self.D_out, bias=True))

        self.criterion = torch.nn.CrossEntropyLoss()
        self.optimizer = torch.optim.SGD(self.model.parameters(), lr=0.1)

    def cross_entropy(self, output, target):
        logits = output[torch.arange(len(output)), target]
        loss = - logits + torch.log(torch.sum(torch.exp(output), axis=-1))
        loss = loss.mean()
        return loss

    # Fase de entrenamiento
    def training (self, inputs, target, epochs, lr):
        for epoch in range(epochs):
            output = self.model(torch.from_numpy(np.array(inputs)).float())
            target = np.array(target)
            if epoch == 0:
                target = np.argmax(target, axis=1)
            error = self.criterion(output, torch.from_numpy(target).long())

            self.l.append((error.item()))

            # Ponemos a 0 los gradiantes
            self.optimizer.zero_grad()

            # Calculamos todos los gradiantes automaticamente
            error.backward()

            # Actualizamos los pesos
            self.optimizer.step()

            print(np.mean(self.l))

    # Calcular predicciones
    def predict (self, inputs):
        print (self.model(torch.from_numpy(np.array(inputs)).float()).sigmoid())
        return self.model(torch.from_numpy(np.array(inputs)).float()).sigmoid().detach().numpy()

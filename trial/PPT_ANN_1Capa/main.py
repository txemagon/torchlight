from Perceptron import Perceptron
from random import choice
from Reglas import Reglas

'''
    Creación de un Modelo capaz de aprender a jugar a Piedra Papel Tijera.
    Tipo de Modelo -> Perceptrón Simple.
    Tipo de entrenamiento -> Supervisado
    Capas del Modelo -> 2 Capas: 3 Neuronas de entrada + neurona bias y 3 neuronas de salida.
    Versión -> v1.0
'''

if __name__ == '__main__':

    # Construir Dataset y Target
    game = Reglas()

    # Construir el modelo perceptrón simple
    ann = Perceptron(9)

    #Entrenamiento
    ann.training(game.inputs, game.gTruth, 7000, 0.1)

    # Pesos y Predicciones
    print("\nPesos Finales:\n", ann.weight)
    print("\nValor del Bias:\n", ann.bias)
    print("\nPredicciones:")

    for i in range(10):
        playerOption = choice(game.inputs)
        print (playerOption)
        print(" Jugador: %s; Modelo: %s" % (game.mapPlayerOption(playerOption), game.mapAnnOption(ann.predict(playerOption))))
import torch
import numpy as np

class AnaP:
    inputs_layer, hidden_layer, output_layer = 4, 4, 3
    er = []

    def __init__(self):
        # Definir arquitectura del modelo neuronal
        self.model = torch.nn.Sequential(
            torch.nn.Linear(self.inputs_layer, self.hidden_layer, bias=True),
            torch.nn.ReLU(),
            torch.nn.Linear(self.hidden_layer, self.output_layer, bias=True),
            torch.nn.Sigmoid(),
            # torch.nn.ReLU(),
            #torch.nn.Linear(self.hidden_layer2, self.output_layer, bias=True)
        )

        # Definir funcion de perdida
        self.loss_function = torch.nn.MSELoss()

        # Definir optimizador
        self.optimizer = torch.optim.SGD(self.model.parameters(), lr=0.1)

    def training(self, inputs, target, epochs):
        # Convertir datasets a tensores
        inputs = torch.from_numpy(np.array(inputs)).float()
        target = torch.from_numpy(np.array(target)).float()

        for epoch in range(epochs):
            # Función de propagación
            output = self.model(inputs)

            # Calculo del error
            error = self.loss_function(output, target)

            # Almacenar valor de error para hacer la media
            self.er.append(error.item())

            # Poner los gradiantes a cero
            self.optimizer.zero_grad()

            # Calcular gradiantes mediante backpropagation (las derivadas finales de los pesos)
            error.backward()

            # Actualizar los pesos mediante el optimizer
            self.optimizer.step()

            # Visualizar media del error
            print(np.mean(self.er), " ", epoch)

    def predict(self, inputs):
        # print(np.mean(self.er))
        # print("outputs", self.model(torch.from_numpy(np.array(inputs)).float()).sigmoid())
        return self.model(torch.from_numpy(np.array(inputs)).float()).sigmoid().detach().numpy()

    def saveDatasets(self, snake, inputs_db_name, target_db_name):
        torch.save(snake.inputs, inputs_db_name + '.pt')
        torch.save(snake.target, target_db_name + '.pt')
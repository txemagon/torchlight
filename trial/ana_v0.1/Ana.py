import numpy as np

class Ana:

    lh_weight = []
    lo_weight = []
    lh_bias   = []
    lo_bias   = []

    def __init__(self, inputs_size, lh_size_weight, lo_size_weight):
        self.lh_weight = np.array(np.random.randn(inputs_size*lh_size_weight))
        self.lo_weight = np.array(np.random.randn(lh_size_weight*lo_size_weight))
        self.lh_bias   = np.array(np.random.randn(lh_size_weight))
        self.lo_bias   = np.array(np.random.randn(lo_size_weight))

        self.lh_weight = self.lh_weight.reshape(4,3)
        self.lo_weight  = self.lo_weight.reshape(3,3)

    def sigmoid (self, input):
        return 1 / (1 + np.exp(-input))

    def sigmoidDer (self, input):
        return self.sigmoid(input) * (1-self.sigmoid(input))

    def propagation(self, inputs):
        lh_inputs  = np.dot(inputs, self.lh_weight)
        lh_outputs = self.sigmoid(lh_inputs)
        lo_inputs  = np.dot(lh_outputs, self.lo_weight)
        return self.sigmoid(lo_inputs)

    def training (self, inputs, target, epochs, lr):
        for epoch in range(epochs):
            lh_inputs  = np.dot(inputs, self.lh_weight) + self.lh_bias
            lh_outputs = self.sigmoid(lh_inputs)
            lo_inputs   = np.dot(lh_outputs, self.lo_weight) + self.lo_bias
            lo_outputs  = self.sigmoid(lo_inputs)

            #print("Outputs ", lo_outputs)
            error = lo_outputs - target
            print ("Error ", error.sum())

            # Fase 1 (Pesos de salida)
            der_lo_input = self.sigmoidDer(lo_inputs)
            #print (np.array(lh_outputs).shape, np.array(error*der_lo_input).shape)
            row, col = np.array(der_lo_input).shape
            der_lo_error = np.dot(lh_outputs, np.asarray(error * der_lo_input).reshape(3,row))

            # Fase 2 (Pesos de capa oculta 2)
            der_lh_error = error * der_lo_input
            der_lh_weight_error = np.dot(der_lh_error, self.lo_weight)
            der_lh_input = self.sigmoidDer(lh_inputs)
            print(np.array(inputs).shape, np.array(der_lh_input * der_lh_weight_error).shape)
            der_lh_final = np.dot(inputs, der_lh_input * der_lh_weight_error)

            lh_bias = error * self.sigmoidDer(self.sigmoid(lh_outputs))
            lo_bias = error * self.sigmoidDer(self.sigmoid(lo_outputs))

            self.lh_weight -= lr * der_lh_final
            self.lo_weight -= lr * der_lo_error

            for i in lh_bias:
                self.lh_bias -= lr * i
            for i in lo_bias:
                self.lo_bias -= lr * i
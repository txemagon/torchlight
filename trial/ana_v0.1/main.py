import signal

import tk as tk
import pygame
import numpy as np
from Ana import Ana
import torch
import random
import dbus

from AnaP import AnaP
from Board import Board
from Fruit import Fruit
from SnakeDistance import Snake



def getDbInputs():
    inputs_size = obj.get_dbus_method('getInputsSize', dbus_interface='local.controller.Controller')()
    inputs = []
    buffer = []
    cont = 0

    for index in range(inputs_size):
        cont += 1
        input = obj.get_dbus_method('getAtInputs', dbus_interface='local.controller.Controller')(index)
        buffer.append(input.conjugate())

        if cont == 4:
            inputs.append(buffer.copy())
            buffer.clear()
            cont = 0
    return inputs

def getDbTarget():
    target_size = obj.get_dbus_method('getTargetSize', dbus_interface='local.controller.Controller')()
    target = []
    buffer = []
    cont = 0

    for index in range(target_size):
        cont += 1
        item_target = obj.get_dbus_method('getAtTarget', dbus_interface='local.controller.Controller')(index)
        buffer.append(item_target.conjugate())

        if cont == 3:
            target.append(buffer.copy())
            buffer.clear()
            cont = 0
    return target


def getInput():
    #DERECHA, IZQUIERDA, ABAJO, ARRIBA
    input = []

    for index in range(4):
        buffer = obj.get_dbus_method('getAtDistance', dbus_interface='local.controller.Controller')(index)
        input.append(buffer.conjugate())

    return input

def sendMove(ann_output):
    # Izquierda
    if ann_output.item(0) > .53:
        direction = 1
    # Derecha
    elif ann_output.item(1) > .53:
         direction = 2
    # Frente
    elif ann_output.item(2) > .53:
        direction = 3

    #print ("direction", direction)
    obj.get_dbus_method('newDirection', dbus_interface='local.controller.Controller')(direction)

def exit_pinia(obj):
    del obj
    exit()

def createInputs(snake):
    direction = np.array(snake.target[len(snake.target) - 1])

    if direction[0] == 1:
        snake.inputs.append([1, 0, 0, 0, fruit.position[0], fruit.position[1],
                            snake.body[0].position[0], snake.body[0].position[1]])

        snake.input = [1, 0, 0, 0, fruit.position[0], fruit.position[1],
                            snake.body[0].position[0], snake.body[0].position[1]]
    elif direction[1] == 1:
        snake.inputs.append([0, 1, 0, 0, fruit.position[0], fruit.position[1],
                            snake.body[0].position[0], snake.body[0].position[1]])

        snake.input = [0, 1, 0, 0, fruit.position[0], fruit.position[1],
                            snake.body[0].position[0], snake.body[0].position[1]]
    elif direction[2] == 1:
        snake.inputs.append([0, 0, 1, 0, fruit.position[0], fruit.position[1],
                            snake.body[0].position[0], snake.body[0].position[1]])

        snake.input = [0, 0, 1, 0, fruit.position[0], fruit.position[1],
                            snake.body[0].position[0], snake.body[0].position[1]]
    elif direction[3] == 1:
        snake.inputs.append([0, 0, 0, 1, fruit.position[0], fruit.position[1],
                            snake.body[0].position[0], snake.body[0].position[1]])

        snake.input = [0, 0, 0, 1, fruit.position[0], fruit.position[1],
                            snake.body[0].position[0], snake.body[0].position[1]]

    #print ("Inputs", snake.inputs[len(snake.inputs)-1])
    #print ("Target", snake.target[len(snake.target)-1])

def saveModel(ana):
    torch.save(ana.model[0].weight, 'w_hidden_layer.pt')
    torch.save(ana.model[2].weight, 'w_output_layer.pt')
    torch.save(ana.model[0].bias, 'b_hidden_layer.pt')
    torch.save(ana.model[2].bias, 'b_output_layer.pt')

def anaTurn():
    for event in pygame.event.get():
        if event.key == pygame.K_SPACE:
            return True

if __name__ == '__main__':

    # Dbus
    # -----------------------------------------------------------------
    bus = dbus.SessionBus()
    obj = bus.get_object('org.torchlight.Inputs', '/inputs')

    while True:
        print("Dataset inputs: ", len(getDbInputs()))
        print("Dataset target: ", len(getDbTarget()))

    # -----------------------------------------------------------------


    list_element = []
    dataset = []
    training = True
    width = 500
    height = 500
    rows = 20 # Divide el tablero en columnas (minimo 1)
    board = Board()
    window = pygame.display.set_mode((width, height))
    snake = Snake((random.randrange(20), random.randrange(20)))
    fruit = Fruit(rows)
    #ana = Ana(4,3,3)
    ana = AnaP()
    clock = pygame.time.Clock()

    #signal.signal(signal.SIGINT, exit_pinia(obj))

    while True:
        clock.tick(5)

        if snake.space and training:
            print ("Inputs", snake.inputs, np.array(snake.inputs).shape)
            print ("Target", snake.target, np.array(snake.target).shape)
            torch.save(snake.inputs, 'inputs_db2.pt')
            torch.save(snake.target, 'target_db2.pt')
            ana.training(torch.load('inputs_db2.pt'), torch.load('target_db2.pt'), 30000)
            #saveModel(ana)
            training = False

        if not training:
            sendMove(ana.predict(getInput()))
            snake.move(fruit, ana.predict(snake.input))
        else:
            sendMove(ana.predict(getInput()))
            snake.move(fruit)
            createInputs(snake)

        # Comprobar si ha comido la fruta
        if snake.body[0].position == fruit.position:
            snake.addCube()
            fruit = Fruit(rows)

        # Comprobar si se ha chocado consigo misma
        for i in range(1, len(snake.body)):
            if snake.body[i].position == snake.getHead().position:
                    parentWindow = tk.Tk() # Ventana padre para el mensaje
                    parentWindow.attributes("-topmost", True) # Atributo para ventana modal
                    parentWindow.withdraw() # De este modo el administrador de ventanas no pinta este widget
                    #messagebox.showinfo('Perdiste primo', 'Menudo paquete')
                    parentWindow.destroy() # Se destrulle la ventana para asignar el foco de vuelta en la padre
                    snake.reset((random.randrange(20), random.randrange(20)))
                    break

        board.redrawWindow(window, snake, fruit, width, rows)


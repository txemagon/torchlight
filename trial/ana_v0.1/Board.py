import pygame

class Board:

    def drawGrid(self, width, rows, window):
        sizeBtwn = width // rows
        x = 0
        y = 0

        for l in range(rows):
            x = x + sizeBtwn
            y = y + sizeBtwn

            pygame.draw.line(window, (200, 200, 200), (x, 0), (x, width))
            pygame.draw.line(window, (200, 200, 200), (0, y), (width, y))

    def redrawWindow(self, window, snake, fruit, width, rows):
        window.fill((27, 24, 28))
        snake.draw(window)
        fruit.draw(window)
        self.drawGrid(width, rows, window)
        pygame.display.update()
import random
import numpy as np
import pygame
from AnaP import AnaP
from BodyPart import BodyPart

class Snake:

    color = (255, 0, 0)
    ana = AnaP()
    old_direction = "right"
    body = []
    input = []
    inputs = []
    target = []
    turns = {}
    space = False
    first = True

    def __init__(self, pos):
        self.head = BodyPart(pos)
        self.body.append(self.head)

    def move(self, fruit, ana_move=np.array([0, 0, 0, 0])):

        if ana_move.sum() == 0:
            direction = 4
        else:
            direction = ana_move.tolist().index(max(ana_move))

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()

        self.caca = True

        keys = pygame.key.get_pressed()

        if keys[pygame.K_a] and not self.old_direction == "right" or direction == 2:
            self.dirnx = -1
            self.dirny = 0
            self.turns[self.head.position[:]] = [self.dirnx, self.dirny]
            self.target.append([0, 0, 1, 0])
            self.old_direction = "left"
            self.first = False
            self.caca = False

        elif keys[pygame.K_d] and not self.old_direction == "left" or direction == 3:
            self.dirnx = 1
            self.dirny = 0
            self.turns[self.head.position[:]] = [self.dirnx, self.dirny]
            self.target.append([0, 0, 0, 1])
            self.old_direction = "right"
            self.first = False
            self.caca = False

        elif keys[pygame.K_w] and not self.old_direction == "down" or direction == 0:
            self.dirnx = 0
            self.dirny = -1
            self.turns[self.head.position[:]] = [self.dirnx, self.dirny]
            self.target.append([1, 0, 0, 0])
            self.old_direction = "up"
            self.first = False
            self.caca = False

        elif keys[pygame.K_s] and not self.old_direction == "up" or direction == 1:
            self.dirnx = 0
            self.dirny = 1
            self.turns[self.head.position[:]] = [self.dirnx, self.dirny]
            self.target.append([0, 1, 0, 0])
            self.old_direction = "down"
            self.first = False
            self.caca = False

        if self.caca == True:
            if self.old_direction == "right":
                self.target.append([0, 0, 0, 1])
            elif self.old_direction == "left":
                self.target.append([0, 0, 1, 0])
            elif self.old_direction == "down":
                self.target.append([0, 1, 0, 0])
            elif self.old_direction == "up":
                self.target.append([1, 0, 0, 0])

        elif self.first == True:
            self.dirnx = 1
            self.dirny = 0
            self.turns[self.head.position[:]] = [self.dirnx, self.dirny]
            self.target.append([0, 0, 0, 1])
            self.old_direction = "right"

        '''
        if keys[pygame.K_a] or self.left:
            if self.direction == 'right':
                self.direction = 'up'
                self.dirnx = 0
                self.dirny = -1
                self.turns[self.head.position[:]] = [self.dirnx, self.dirny]

            elif self.direction == 'up':
                self.direction = 'left'
                self.dirnx = -1
                self.dirny = 0
                self.turns[self.head.position[:]] = [self.dirnx, self.dirny]

            elif self.direction == 'down':
                self.direction = 'right'
                self.dirnx = 1
                self.dirny = 0
                self.turns[self.head.position[:]] = [self.dirnx, self.dirny]

            elif self.direction == 'left':
                self.direction = 'down'
                self.dirnx = 0
                self.dirny = 1
                self.turns[self.head.position[:]] = [self.dirnx, self.dirny]
            self.left = False
            self.target.append([1,0,0])

        elif keys[pygame.K_d] or self.right:
            if self.direction == 'right':
                self.direction = 'down'
                self.dirnx = 0
                self.dirny = 1
                self.turns[self.head.position[:]] = [self.dirnx, self.dirny]

            elif self.direction == 'up':
                self.direction = 'right'
                self.dirnx = 1
                self.dirny = 0
                self.turns[self.head.position[:]] = [self.dirnx, self.dirny]

            elif self.direction == 'left':
                self.direction = 'up'
                self.dirnx = 0
                self.dirny = -1
                self.turns[self.head.position[:]] = [self.dirnx, self.dirny]

            elif self.direction == 'down':
                self.direction = 'left'
                self.dirnx = -1
                self.dirny = 0
                self.turns[self.head.position[:]] = [self.dirnx, self.dirny]
            self.right = False
            self.target.append([0,1,0])
        '''
        #else:
        #self.target.append([0,0,1])

        #self.wallDistance()
        #self.whoseFront()
        #self.fruitDistance(fruit)
        #self.createDataset()

        if keys[pygame.K_SPACE]:
            self.space = True

        for i, c in enumerate(self.body):

            p = c.position[:]

            if p in self.turns:

                turn = self.turns[p]

                c.move(turn[0], turn[1])

                if i == len(self.body) - 1:
                    self.turns.pop(p)
                '''
            else:

                if c.dirnx == -1 and c.position[0] <= 0:
                    c.position = (c.rows - 1, c.position[1])

                elif c.dirnx == 1 and c.position[0] >= c.rows - 1:
                    c.position = (0, c.position[1])

                elif c.dirny == 1 and c.position[1] >= c.rows - 1:
                    c.position = (c.position[0], 0)

                elif c.dirny == -1 and c.position[1] <= 0:
                    c.position = (c.position[0], c.rows - 1)

                else:
                    c.move(c.dirnx, c.dirny)
                '''
            else:
                if c.dirnx == -1 and c.position[0] <= 0:
                    self.reset((random.randrange(20), random.randrange(20)))
                elif c.dirnx == 1 and c.position[0] >= c.rows - 1:
                    self.reset((random.randrange(20), random.randrange(20)))
                elif c.dirny == 1 and c.position[1] >= c.rows - 1:
                    self.reset((random.randrange(20), random.randrange(20)))
                elif c.dirny == -1 and c.position[1] <= 0:
                    self.reset((random.randrange(20), random.randrange(20)))
                else:
                    c.move(c.dirnx, c.dirny)

    def wallDistance(self):
        self.WxRight = 20 - self.body[0].position[0]
        self.WxLeft = self.body[0].position[0] + 1

        self.WyUp = self.body[0].position[1] + 1
        self.WyDown = 20 - self.body[0].position[1]

    def fruitDistance(self, fruit):
        # Eje x
        if self.body[0].position[0] == fruit.position[0]:
            if self.direction == 'right':
                for position in range(self.body[0].position[1], 20, 1):
                    # Fruta en la derecha
                    if position == fruit.position[1]:
                        self.fruitLeft = 0
                        self.fruitRight = fruit.position[1] - self.body[0].position[1]
                        self.fruitBack = 0
                        self.fruitFront = 0
                        break
                    else:
                        self.fruitLeft = self.body[0].position[1] - fruit.position[1]
                        self.fruitRight = 0
                        self.fruitBack = 0
                        self.fruitFront = 0
            elif self.direction == 'left':
                # Fruta en la izquierda
                for position in range(self.body[0].position[1], 20, 1):
                    if position == fruit.position[1]:
                        self.fruitLeft = fruit.position[1] - self.body[0].position[1]
                        self.fruitRight = 0
                        self.fruitBack = 0
                        self.fruitFront = 0
                        break
                    else:
                        self.fruitLeft = 0
                        self.fruitRight = self.body[0].position[1] - fruit.position[1]
                        self.fruitBack = 0
                        self.fruitFront = 0
            elif self.direction == 'up':
                # Fruta detras
                for position in range(self.body[0].position[1], 20, 1):
                    if position == fruit.position[1]:
                        self.fruitLeft = 0
                        self.fruitRight = 0
                        self.fruitBack = fruit.position[1] - self.body[0].position[1]
                        self.fruitFront = 0
                        break
                    else:
                        self.fruitLeft = 0
                        self.fruitRight = 0
                        self.fruitBack = 0
                        self.fruitFront = self.body[0].position[1] - fruit.position[1]
            elif self.direction == 'down':
                # Fruta delante
                for position in range(self.body[0].position[1], 20, 1):
                    if position == fruit.position[1]:
                        self.fruitLeft = 0
                        self.fruitRight = 0
                        self.fruitBack = 0
                        self.fruitFront = fruit.position[1] - self.body[0].position[1]
                        break
                    else:
                        self.fruitLeft = 0
                        self.fruitRight = 0
                        self.fruitBack = self.body[0].position[1] - fruit.position[1]
                        self.fruitFront = 0

        # Eje y
        elif self.body[0].position[1] == fruit.position[1]:
            if self.direction == 'right':
                # Fruta delante
                for position in range(self.body[0].position[0], 20, 1):
                    if position == fruit.position[0]:
                        self.fruitLeft = 0
                        self.fruitRight = 0
                        self.fruitBack = 0
                        self.fruitFront = fruit.position[0] - self.body[0].position[0]
                        break
                    else:
                        self.fruitLeft = 0
                        self.fruitRight = 0
                        self.fruitBack = self.body[0].position[0] - fruit.position[0]
                        self.fruitFront = 0
            elif self.direction == 'left':
                # Fruta detras
                for position in range(self.body[0].position[0], 20, 1):
                    if position == fruit.position[0]:
                        self.fruitLeft = 0
                        self.fruitRight = 0
                        self.fruitBack = fruit.position[0] - self.body[0].position[0]
                        self.fruitFront = 0
                        break
                    else:
                        self.fruitLeft = 0
                        self.fruitRight = 0
                        self.fruitBack = 0
                        self.fruitFront = self.body[0].position[0] - fruit.position[0]
                        break
            elif self.direction == 'up':
                # Fruta a la derecha
                for position in range(self.body[0].position[0], 20, 1):
                    if position == fruit.position[0]:
                        self.fruitLeft = 0
                        self.fruitRight = fruit.position[0] - self.body[0].position[0]
                        self.fruitBack = 0
                        self.fruitFront = 0
                        break
                    else:
                        self.fruitLeft = self.body[0].position[0] - fruit.position[0]
                        self.fruitRight = 0
                        self.fruitBack = 0
                        self.fruitFront = 0
            elif self.direction == 'down':
                # Fruta a la izquierda
                for position in range(self.body[0].position[0], 20, 1):
                    if position == fruit.position[0]:
                        self.fruitLeft = fruit.position[0] - self.body[0].position[0]
                        self.fruitRight = 0
                        self.fruitBack = 0
                        self.fruitFront = 0
                        break
                    else:
                        self.fruitLeft = 0
                        self.fruitRight = self.body[0].position[0] - fruit.position[0]
                        self.fruitBack = 0
                        self.fruitFront = 0

        else:
            self.fruitLeft = 0
            self.fruitRight = 0
            self.fruitBack = 0
            self.fruitFront = 0

        #print (self.fruitLeft,self.fruitRight,self.fruitBack,self.fruitFront)

    def whoseFront(self):
        if self.direction == 'right':
            self.snakeLeft = self.WyUp
            self.snakeRight = self.WyDown
            self.snakeBack = self.WxLeft
            self.snakeFront = self.WxRight

        if self.direction == 'left':
            self.snakeLeft = self.WyDown
            self.snakeRight = self.WyUp
            self.snakeBack = self.WxRight
            self.snakeFront = self.WxLeft

        if self.direction == 'up':
            self.snakeLeft = self.WxLeft
            self.snakeRight = self.WxRight
            self.snakeBack = self.WyDown
            self.snakeFront = self.WyUp

        if self.direction == 'down':
            self.snakeLeft = self.WxRight
            self.snakeRight = self.WxLeft
            self.snakeBack = self.WyUp
            self.snakeFront = self.WyDown

    def createDataset(self):
        self.inputs.append([self.snakeFront, self.snakeBack, self.snakeLeft, self.snakeRight,
                            self.fruitFront, self.fruitBack, self.fruitLeft, self.fruitRight])

        self.input = [self.snakeFront, self.snakeBack, self.snakeLeft, self.snakeRight,
                      self.fruitFront, self.fruitBack, self.fruitLeft, self.fruitRight]

    def reset(self, pos):

        self.head = BodyPart(pos)

        self.body = []

        self.body.append(self.head)

        self.turns = {}

        self.dirnx = 0

        self.dirny = 1

    def addCube(self):

        tail = self.body[-1]

        dx, dy = tail.dirnx, tail.dirny

        if dx == 1 and dy == 0:

            self.body.append(BodyPart((tail.position[0] - 1, tail.position[1])))

        elif dx == -1 and dy == 0:

            self.body.append(BodyPart((tail.position[0] + 1, tail.position[1])))

        elif dx == 0 and dy == 1:

            self.body.append(BodyPart((tail.position[0], tail.position[1] - 1)))

        elif dx == 0 and dy == -1:

            self.body.append(BodyPart((tail.position[0], tail.position[1] + 1)))

        self.body[-1].dirnx = dx

        self.body[-1].dirny = dy

    def draw(self, window):

        for i, c in enumerate(self.body):

            if i == 0:

                c.draw(window, True)

            else:

                c.draw(window)

    def getHead(self):
        return self.head
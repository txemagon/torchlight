import pygame

class BodyPart:

    color = (255, 0, 0)
    rows = 20
    weight = 500

    def __init__(self, pos):
        self.position = pos
        # dirección inicial de la serpiente
        self.dirnx = 1
        self.dirny = 0

    def move(self, dirnx, dirny):
        self.dirnx = dirnx
        self.dirny = dirny
        self.position = (self.position[0] + self.dirnx, self.position[1] + self.dirny)

    def draw(self, surface, eyes=False):
        dis = self.weight // self.rows
        x = self.position[0]
        y = self.position[1]

        pygame.draw.rect(surface, self.color, (x * dis + 1, y * dis + 1, dis - 2, dis - 2))

        # Pintar Ojos
        if eyes:
            centre = dis // 2
            radius = 3
            circleMiddle = (x * dis + centre - radius, y * dis + 8)
            circleMiddle2 = (x * dis + dis - radius * 2, y * dis + 8)

            pygame.draw.circle(surface, (0, 0, 0), circleMiddle, radius)
            pygame.draw.circle(surface, (0, 0, 0), circleMiddle2, radius)
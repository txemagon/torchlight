import random
import pygame

class Fruit:

    color = (0, 255, 0)
    rows = 20
    weight = 500

    def __init__(self, rows):
        x = random.randrange(rows)
        y = random.randrange(rows)
        self.position = (x, y)

    def draw(self, window):
        dis = self.weight // self.rows # // devuelve el entero de una division (25px)
        x = self.position[0]
        y = self.position[1]

        pygame.draw.rect(window, self.color, (x * dis + 1, y * dis + 1, dis - 2, dis - 2))